![](https://img.shields.io/badge/license-MIT-green)
![](https://img.shields.io/badge/OSI-This%20repository%20is%20free%20software-42a0ff?logo=open-source-initiative&logoColor=white)

## Demo video:

[![Demonstration](https://img.youtube.com/vi/JEK-QkHWQ6k/0.jpg)](https://www.youtube.com/watch?v=JEK-QkHWQ6k)

## How to run:
    
    python3 script.py

## Pre-requisites:

    - python3 (python2 untested)
    - pip install pynput
    
## Featuers:

    - 45 different SFX for key up and key down
    - separate SFX for spacebar
    
## Notes:

    - This project is just a proof-of-concept
    - It has been tested on both Windows and Linux
    - Sometimes sounds may have a delay before kicking in if the keyboard has been left unattended for a while
    - Audio quality may not be perfect, but quickly typing stuff sounds pretty good
    - The playsound.py file originates from https://github.com/TaylorSMarks/playsound, but is slightly modified