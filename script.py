import threading
from pynput import keyboard
import time, os, random
from playsound import playsound

def getPath ():
	return os.path.dirname (os.path.realpath (__file__))

keyPresses = {}
isPressing = {}
keymapPress = {}
keymapRelease = {}

pressFolder = os.listdir (getPath () + "/SFX/press")
releaseFolder = os.listdir (getPath () + "/SFX/release")

customs = [keyboard.Key.space]
customFolders = {}
for key in customs:
	customFolders[key] = {"press": os.listdir (("{}/SFX/custom/{}/press".format (getPath (), key))),
		"release": os.listdir (("{}/SFX/custom/{}/release".format (getPath (), key)))}

def playSFX (fname):
	try:
		thr = threading.Thread (target = playsound, args = (getPath () + "/SFX" + fname,))
		thr.daemon = True
		thr.start ()
	except Exception as e:
		print (e)

def refreshKeySounds (key):
	if (key in customs):
		keymapPress[key] = "/custom/" + str(key) + "/press/" + random.choice (customFolders[key]["press"])
		keymapRelease[key] = "/custom/" + str(key) + "/release/" + random.choice (customFolders[key]["release"])
	else:
		keymapPress[key] = "/press/" + random.choice (pressFolder)
		keymapRelease[key] = "/release/" + random.choice (releaseFolder)

def press (key):
	if (key in isPressing and isPressing[key] == 1):
		return

	if (key not in keyPresses):
		keyPresses[key] = 0

	keyPresses[key] += 1
	if (key not in keymapPress or keyPresses[key] >= 4 or key in customs):
		keyPresses[key] = 1
		refreshKeySounds (key)

	isPressing[key] = 1
	playSFX (keymapPress[key])

def release (key):
	isPressing[key] = 0
	if (key not in keymapRelease):
		refreshKeySounds (key)

	playSFX (keymapRelease[key])

listener = keyboard.Listener (
    on_press = press,
    on_release = release)
listener.start ()

while True:
	time.sleep (5)